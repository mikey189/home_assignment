import bl.CalculateMonotonicFunction;
import org.junit.Assert;
import org.junit.Test;
import pojos.MonotonicCalcResult;
import pojos.TrafficData;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CalculateMonotonicFunctionTest {
	CalculateMonotonicFunction calculateMonotonicFunctionTest = new CalculateMonotonicFunction();

	@Test
	public void monotonicDataList_thatContainsOneObject_shouldReturnFalse()
	{
		List<MonotonicCalcResult> calculationList = new ArrayList<>();
		List<Tuple2<String, Iterable<TrafficData>>> dataToTest = new ArrayList<>();

		dataToTest.add(new Tuple2<>("site1", new ArrayList<TrafficData>()
		{{
			add(new TrafficData("2018-01-02T00:00:00.000Z", "site1", 1));
		}}));

		Iterator<MonotonicCalcResult> result = calculateMonotonicFunctionTest.call(dataToTest.iterator());

		Assert.assertNotNull(result);

		result.forEachRemaining(calculationList::add);

		Assert.assertEquals(calculationList.size(),1);

		MonotonicCalcResult monotonicCalcResult = calculationList.get(0);

		Assert.assertFalse(monotonicCalcResult.isMonotonic());
	}

	@Test
	public void monotonicDataList_thatContainsMonotonicData_shouldReturnTrue()
	{
		List<MonotonicCalcResult> calculationList = new ArrayList<>();
		List<Tuple2<String, Iterable<TrafficData>>> dataToTest = new ArrayList<>();

		dataToTest.add(new Tuple2<>("site1", new ArrayList<TrafficData>()
		{{
			add(new TrafficData("2018-01-02T00:00:00.000Z", "site1", 1));
			add(new TrafficData("2018-01-02T01:00:00.000Z", "site1", 2));
		}}));

		Iterator<MonotonicCalcResult> result = calculateMonotonicFunctionTest.call(dataToTest.iterator());

		Assert.assertNotNull(result);

		result.forEachRemaining(calculationList::add);

		Assert.assertEquals(calculationList.size(),1);

		MonotonicCalcResult monotonicCalcResult = calculationList.get(0);

		Assert.assertTrue(monotonicCalcResult.isMonotonic());
	}

	@Test
	public void monotonicDataList_thatDoesNotContainsMonotonicData_ShouldReturnTrue()
	{
		List<MonotonicCalcResult> calculationList = new ArrayList<>();
		List<Tuple2<String, Iterable<TrafficData>>> dataToTest = new ArrayList<>();

		dataToTest.add(new Tuple2<>("site1", new ArrayList<TrafficData>()
		{{
			add(new TrafficData("2018-01-02T00:00:00.000Z", "site1", 5));
			add(new TrafficData("2018-01-02T01:00:00.000Z", "site1", 2));
		}}));

		Iterator<MonotonicCalcResult> result = calculateMonotonicFunctionTest.call(dataToTest.iterator());

		Assert.assertNotNull(result);

		result.forEachRemaining(calculationList::add);

		Assert.assertEquals(calculationList.size(),1);

		MonotonicCalcResult monotonicCalcResult = calculationList.get(0);

		Assert.assertFalse(monotonicCalcResult.isMonotonic());
	}
}