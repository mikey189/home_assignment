package common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pojos.MonotonicCalcResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Utils
{
	private static final String            RESULT_FILE         = "is_monotonic";
	private static final String            RESULT_EXTENSION    = "json";
	private static final String            DATE_FORMAT         = "yyyy-MM-dd'T'HH-mm-ss'Z'";
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
	private static       ObjectMapper      objectMapper        = new ObjectMapper();

	public static void saveResultToOutputFolder(String outputPath, List<MonotonicCalcResult> monotonicCalcResults)
	{
		String json = convertToJson(monotonicCalcResults);

		if (json == null)
		{
			System.err.println("Failed to save result to output path");
			return;
		}

		String date =  LocalDateTime.now().format(DATE_TIME_FORMATTER);
		String fileName = String.format("%s_%s.%s", RESULT_FILE, date, RESULT_EXTENSION);
		try
		{
			Files.write(Paths.get(outputPath, fileName), json.getBytes());
		}
		catch (IOException e)
		{
			System.err.println("Failed to write json result to file, error = " + e);
		}
	}

	private static String convertToJson(List<MonotonicCalcResult> monotonicCalcResults)
	{
		try
		{
			return objectMapper.writeValueAsString(monotonicCalcResults);
		}
		catch (JsonProcessingException e)
		{
			System.err.println("Failed to convert pojos.MonotonicCalcResult to json, error = " + e.getMessage());
			return null;
		}
	}

}
