import bl.CalculateMonotonicFunction;
import bl.LineToRowConverterFunction;
import common.Utils;
import grouping.GroupByFunction;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import pojos.MonotonicCalcResult;
import pojos.TrafficData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class CalculateMonotonicApp
{
	public static void main(String[] args)
	{
		SparkSession spark;

		if (args.length < 2)
		{
			System.err.println("Usage: Streaming <input folder> <output folder>");
			System.exit(1);
		}

		for (int i = 0; i < args.length; i++)
		{
			System.out.println("Argument " + i + ": " + args[i]);
		}

		String inputPath = args[args.length-2];
		String outputPath = args[args.length-1];

		// local mode running within IDE so using first 2 arguments
		if (args.length == 2)
		{
			spark = SparkSession.builder()
					.appName("HomeWorkAssignment")
					.master("local[*]")
					.config("spark.driver.bindAddress", "127.0.0.1")
					.getOrCreate();
		}
		else
		{
			spark = SparkSession.builder()
					.appName("HomeWorkAssignment")
					.getOrCreate();
		}

		try
		{
			if (spark == null){
				System.err.println("Spark context wasn't initialized properly");
				System.exit(1);
			}
			SparkSession sparkSession = spark.cloneSession();
			FileSystem fs = FileSystem.get(sparkSession.sparkContext().hadoopConfiguration());

			if (!fs.exists(new Path(inputPath)))
			{
				System.err.println("Input folder doesn't exist, please enter valid folder");
				System.exit(1);
			}

			if (!fs.exists(new Path(outputPath)))
			{
				System.err.println("Output folder doesn't exist, please enter valid folder");
				System.exit(1);
			}

			StructType inputSchema = new StructType(
					new StructField[] { new StructField("timestamp", DataTypes.StringType, false, Metadata.empty()),
							new StructField("website", DataTypes.StringType, false, Metadata.empty()),
							new StructField("users", DataTypes.IntegerType, false, Metadata.empty()) });

			// read lines from json
			JavaRDD<Row> jsonValues = spark.read().schema(inputSchema).json(inputPath).javaRDD();

			// convert json objects to pojos.TrafficData objects
			LineToRowConverterFunction lineToRowConverterFunction = new LineToRowConverterFunction();
			//flatMapToPair
			JavaRDD<TrafficData> trafficDataset = jsonValues.map(lineToRowConverterFunction);

			// group DataSet by grouping function, which is using 'website' as key
			JavaPairRDD<String, Iterable<TrafficData>> stringIterableJavaPairRDD = trafficDataset.groupBy(new GroupByFunction());

			// run calculation on each group data by mapGroups operation
			CalculateMonotonicFunction calculateMonotonicFunction = new CalculateMonotonicFunction();
			JavaRDD<MonotonicCalcResult> monotonicCalcResultJavaRDD = stringIterableJavaPairRDD.mapPartitions(calculateMonotonicFunction);

			List<MonotonicCalcResult> monotonicCalcResult = monotonicCalcResultJavaRDD.collect();

			// save output to folder
			Utils.saveResultToOutputFolder(outputPath, monotonicCalcResult);

			spark.stop();
		}
		catch (IOException e)
		{
			System.err.println("Failed to generate FileSystem, error = " + e.getMessage());
		}
	}
}