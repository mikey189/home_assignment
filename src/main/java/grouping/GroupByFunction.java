package grouping;

import org.apache.spark.api.java.function.Function;
import pojos.TrafficData;

public class GroupByFunction implements Function<TrafficData, String>
{
	/** returns the key that will be used for grouping the traffic data
	 *
	 * @param trafficData
	 * @return Grouping Key
	 */
	public String call(TrafficData trafficData)
	{
		return trafficData.getWebSite();
	}
}
