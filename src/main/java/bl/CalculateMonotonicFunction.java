package bl;

import org.apache.spark.api.java.function.FlatMapFunction;
import pojos.MonotonicCalcResult;
import pojos.TrafficData;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CalculateMonotonicFunction implements FlatMapFunction<Iterator<Tuple2<String, Iterable<TrafficData>>>, MonotonicCalcResult>
{
	/**
	 * Calculates if site data is increasing monotonically
	 * @param pairSiteWithDataIterator
	 * @return Iterator of pojos.MonotonicCalcResult
	 * @see MonotonicCalcResult
	 */
	@Override
	public Iterator<MonotonicCalcResult> call(Iterator<Tuple2<String, Iterable<TrafficData>>> pairSiteWithDataIterator)
	{
		List<MonotonicCalcResult> monotonicCalcResults = new ArrayList<>();
		while (pairSiteWithDataIterator.hasNext())
		{
			Tuple2<String, Iterable<TrafficData>> group = pairSiteWithDataIterator.next();

			if (group != null && group._2 != null)
			{
				boolean isMonotonic = true;
				MonotonicCalcResult monotonicCalcResult = new MonotonicCalcResult(group._1, false);
				monotonicCalcResults.add(monotonicCalcResult);

				List<TrafficData> trafficList = new ArrayList<>();

				// converting traffic data iterator into list
				group._2.iterator().forEachRemaining(trafficList::add);

				// sort the traffic data in the list by timeStamp
				trafficList.sort(Comparator.comparing(TrafficData::getTimeStamp));

				// list contains only one item, so can't be monotonic
				if (trafficList.size() == 1){
					break;
				}

				// iterate list to check if users value increasing
				for (int i = 1; i < trafficList.size(); i++)
				{
					if (trafficList.get(i-1).getUsers() > trafficList.get(i).getUsers())
					{
						// the traffic is already not monotonic
						isMonotonic = false;
					}
				}

				monotonicCalcResult.setMonotonic(isMonotonic);
			}
		}
		return monotonicCalcResults.iterator();
	}
}
