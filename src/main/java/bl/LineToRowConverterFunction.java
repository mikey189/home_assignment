package bl;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;
import pojos.TrafficData;

public class LineToRowConverterFunction implements Function<Row, TrafficData>
{
	/** Extract data from collection of rows and converts to Traffic data objects
	 *
	 * @param row
	 * @return Traffic data
	 */
	@Override
	public TrafficData call(Row row)
	{
		String timestamp = row.getAs("timestamp");
		String website = row.getAs("website");
		Integer users = row.getAs("users");
		return new TrafficData(timestamp,website,users);
	}
}
