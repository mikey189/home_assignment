package pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrafficData implements Serializable
{
	String timeStamp;
	String webSite;
	Integer users;
}
