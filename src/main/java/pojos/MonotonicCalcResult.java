package pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MonotonicCalcResult implements Serializable
{
	@JsonProperty
	String website;

	@JsonProperty
	boolean monotonic;
}
