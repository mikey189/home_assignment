# Home Work Assignment

Given an input of json (lines) files, whose json objects have ‘timestamp’,
‘website’ and ‘users’ fields, the application will
check whether ‘users’ as a function of ‘timestamp’ is a monotonically
increasing function for each ‘website’.

## Input example
```json
{“timestamp”: “2018-01-02 T00:00:00.000Z ”, “website”: “sitea”, “users”: 13}
{“timestamp”: “2020-05-23 T00:00:00.000Z ”, “website”: “sitea”, “users”: 11000}
```

## Execution instructions
1. Create input folder that includes a json file with the schema:
```json
 {“timestamp”: string, “website”: string, “users”: integer}
```
 
2. Create output folder 
 
3. Run  `mvn clean install`, or use the included jars in the `target` folder

4. Run the application with spark-submit, for example execution with a local mode:
 `spark-submit --master local[*]  --deploy-mode client  --num-executors 1  --driver-cores 1  --driver-memory 2G  --executor-cores 2  --executor-memory 2G  --name homeworkAssignment  --class CalculateMonotonicApp  ./target/* CalculateMonotonicApp /opt/assignment/in /out/assignment/out`
    
### Nice to have  
- Add logging mechanism instead of using stdout/stderr
- Make the application to work in Batches/Structured Streaming instead of single run
- Accumulations of the data 